use crate::parser::{Ident, token, lit, LitExpr, ident, parse_scope, IdentScope};
use crate::lexer::lit::Lit;
use crate::parser::stmt::Stmt;
use crate::parser::ast::{InfixOp, Precedence, infix_op};
use crate::lexer::token::{Tokens, Token};
use nom::IResult;
use nom::Err;
use nom::multi::many0;
use nom::branch::alt;
use nom::combinator::{opt, map};
use nom::bytes::complete::take;
use nom::error::ErrorKind;

#[derive(Debug, PartialEq, Clone)]
pub enum Expr {
    Ident(Ident),
    Lit(LitExpr),
    Infix(InfixExpr),
    Call(CallExpr),
}

#[derive(Debug, PartialEq, Clone)]
pub struct InfixExpr {
    pub left: Box<Expr>,
    pub op: InfixOp,
    pub right: Box<Expr>
}

#[derive(Debug, PartialEq, Clone)]
pub struct CallExpr {
    pub _fn: Ident,
    pub args: Vec<Expr>,
}

fn parse_comma_expr(i: Tokens) -> IResult<Tokens, Expr> {
    let (i, _) = token(Token::Comma)(i)?;
    parse_expr(i)
}

fn parse_exprs(i: Tokens) -> IResult<Tokens, Vec<Expr>> {
    let (i, e) = parse_expr(i)?;
    let (i, es) = many0(parse_comma_expr)(i)?;
    Ok((i, [&vec![e], &es[..]].concat()))
}

fn parse_lit_expr(i: Tokens) -> IResult<Tokens, Expr> {
    let (i, lit) = lit(i)?;
    Ok((i, Expr::Lit(lit)))
}

fn parse_ident_expr(i: Tokens) -> IResult<Tokens, Expr> {
    let (i, name) = ident(i)?;
    Ok((i, Expr::Ident(Ident { name, scope: IdentScope::Variable })))
}

fn parse_atom_expr(i: Tokens) -> IResult<Tokens, Expr> {
    alt((
        parse_lit_expr,
        parse_ident_expr,
        ))(i)
}

fn parse_call_expr(i: Tokens, _fn: Expr) -> IResult<Tokens, Expr> {
    let mut _fn = match _fn {
        Expr::Ident(ident) => ident,
        _ => return Err(Err::Error((i, ErrorKind::Tag)))
    };
    _fn.scope = IdentScope::Function;

    let (i, _) = token(Token::LPar)(i)?;
    let (i, args) = alt((
        parse_exprs,
        empty_vec
        ))(i)?;
    let (i, _) = token(Token::RPar)(i)?;
    Ok((i, Expr::Call(CallExpr { _fn, args })))
}

fn parse_infix_expr(i: Tokens, left: Expr) -> IResult<Tokens, Expr> {
    let (_i, t) = opt(take(1usize))(i)?;

    match t {
        Some(t) => {
            let tok = &t.tok[0];
            let (prec, op) = infix_op(tok);
            match op {
                None => Err(Err::Error((i, ErrorKind::Tag))),
                Some(op) => {
                    let (i, right) = parse_pratt_expr(_i, prec)?;
                    Ok((i, Expr::Infix(InfixExpr { left: Box::new(left), op, right: Box::new(right) })))
                }
            }
        }
        None => Err(Err::Error((i, ErrorKind::Tag)))
    }
}

pub fn parse_expr(i: Tokens) -> IResult<Tokens, Expr> {
    parse_pratt_expr(i, Precedence::Lowest)
}

fn parse_pratt_expr(i: Tokens, precedence: Precedence) -> IResult<Tokens, Expr> {
    let (i, left) = parse_atom_expr(i)?;
    parse_pratt_expr_full(i, precedence, left)
}

fn parse_pratt_expr_full(i: Tokens, precedence: Precedence, left: Expr) -> IResult<Tokens, Expr> {
    let (_i, t) = opt(take(1usize))(i)?;
    match t {
        Some(t) => {
            let tok = t.tok[0].clone();

            match infix_op(&tok) {
                (Precedence::Call, _) if precedence < Precedence::Call => {
                    let (i, left) = parse_call_expr(i, left)?;
                    parse_pratt_expr_full(i, precedence, left)
                }
                (ref peek_precedence, _) if precedence < *peek_precedence => {
                    let (i, left) = parse_infix_expr(i, left)?;
                    parse_pratt_expr_full(i, precedence, left)
                }
                _ => Ok((i, left))
            }
        }
        None => Ok((_i, left)),
    }
}

fn empty_vec(i: Tokens) -> IResult<Tokens, Vec<Expr>> {
    Ok((i, vec![]))
}
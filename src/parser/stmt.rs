use crate::parser::{Ident, Ty};
use crate::parser::expr::Expr;

#[derive(Debug, PartialEq, Clone)]
pub enum Stmt {
    Def(DefStmt),
    Let(LetStmt),
    Ret(RetStmt),
    If(IfStmt),
    Expr(Expr)
}

#[derive(Debug, PartialEq, Clone)]
pub struct IfStmt {
    pub cond: Expr,
    pub body: Vec<Stmt>,
    pub elif: Vec<Stmt>
}

#[derive(Debug, PartialEq, Clone)]
pub struct DefStmt {
    pub ident: Ident,
    pub args: Vec<Arg>,
    pub ret: Ty,
    pub body: Vec<Stmt>
}

#[derive(Debug, PartialEq, Clone)]
pub struct Arg {
    pub name: Ident,
    pub ty: Ty
}

#[derive(Debug, PartialEq, Clone)]
pub struct RetStmt {
    pub val: Expr,
}

#[derive(Debug, PartialEq, Clone)]
pub struct LetStmt {
    pub ident: Ident,
    pub val: Expr
}


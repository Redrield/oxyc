use crate::lexer::token::Token;

#[derive(Debug, PartialEq, Clone)]
pub enum InfixOp {
    Eq,
    NotEq,
    GtEq,
    LtEq,
    Gt,
    Lt,
    Add,
    Sub,
    Mul,
    Div
}

#[derive(Debug, Clone, PartialOrd, PartialEq)]
pub enum Precedence {
    Lowest,
    EqCmp,
    OrdCmp,
    Additive,
    Multiplicative,
    Call,
    Idx,
}


pub fn infix_op(tok: &Token) -> (Precedence, Option<InfixOp>) {
    match *tok {
        Token::Eq => (Precedence::EqCmp, Some(InfixOp::Eq)),
        Token::Ne => (Precedence::EqCmp, Some(InfixOp::NotEq)),
        Token::Ge => (Precedence::OrdCmp, Some(InfixOp::GtEq)),
        Token::Le => (Precedence::OrdCmp, Some(InfixOp::LtEq)),
        Token::Gt => (Precedence::OrdCmp, Some(InfixOp::Gt)),
        Token::Lt => (Precedence::OrdCmp, Some(InfixOp::Lt)),
        Token::Plus => (Precedence::Additive, Some(InfixOp::Add)),
        Token::Minus => (Precedence::Additive, Some(InfixOp::Sub)),
        Token::Mul => (Precedence::Multiplicative, Some(InfixOp::Mul)),
        Token::Div => (Precedence::Multiplicative, Some(InfixOp::Div)),
        Token::LPar => (Precedence::Call, None),
        _ => (Precedence::Lowest, None)
    }
}


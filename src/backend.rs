use crate::parser::expr::*;
use crate::parser::stmt::*;
use crate::parser::{Ident, LitExpr};

pub mod tag;
pub mod validation;
pub mod tagged_ast;

pub trait Visitor {
    fn visit_expr(&mut self, expr: &Expr) {
        match expr {
            Expr::Ident(ident) => self.visit_ident_expr(ident),
            Expr::Lit(expr) => self.visit_lit_expr(expr),
            Expr::Infix(expr) => self.visit_infix_expr(expr),
            Expr::Call(expr) => self.visit_call_expr(expr),
        }
    }

    fn visit_ident_expr(&mut self, ident: &Ident) {}

    fn visit_lit_expr(&mut self, expr: &LitExpr) {}

    fn visit_infix_expr(&mut self, expr: &InfixExpr) {}

    fn visit_call_expr(&mut self, expr: &CallExpr) {}

    fn visit_stmt(&mut self, stmt: &Stmt) {
        match stmt {
            Stmt::Def(stmt) => self.visit_def_stmt(stmt),
            Stmt::Let(stmt) => self.visit_let_stmt(stmt),
            Stmt::Ret(stmt) => self.visit_ret_stmt(stmt),
            Stmt::If(stmt) => self.visit_if_stmt(stmt),
            Stmt::Expr(expr) => self.visit_expr(expr),
        }
    }

    fn visit_def_stmt(&mut self, stmt: &DefStmt) {}

    fn visit_let_stmt(&mut self, stmt: &LetStmt) {}

    fn visit_ret_stmt(&mut self, stmt: &RetStmt) {}

    fn visit_if_stmt(&mut self, stmt: &IfStmt) {}
}
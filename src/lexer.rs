pub mod token;
pub mod lit;
mod op;
mod keywords;

use self::token::*;
use self::lit::*;
use self::op::*;
use self::keywords::*;

use nom::ws;
use nom::IResult;
use nom::branch::alt;
use nom::combinator::{map, recognize, not, verify, opt};
use nom::bytes::complete::{tag, take, take_while};
use nom::character::is_alphabetic;
use nom::character::complete::{alphanumeric0, multispace0};
use nom::multi::{many0, separated_list, separated_listc};
use nom::sequence::delimited;
use std::iter::Iterator;

fn parse_paren(input: &str) -> IResult<&str, Token> {
    alt((
        map(tag("("), |_| Token::LPar),
        map(tag(")"), |_| Token::RPar)
    ))(input)
}

fn parse_brace(input: &str) -> IResult<&str, Token> {
    alt((
        map(tag("{"), |_| Token::LBrace),
        map(tag("}"), |_| Token::RBrace)
    ))(input)
}

fn parse_atom(input: &str) -> IResult<&str, Token> {
    alt((
        parse_paren,
        parse_brace,
        parse_op,
        map(tag(";"), |_| Token::Semi),
        map(tag(","), |_| Token::Comma),
        map(tag(":"), |_| Token::Colon)
    ))(input)
}

fn parse_ident(input: &str) -> IResult<&str, Token> {
    recognize(not(parse_reserved))(input)
        .and_then(|(o, _)| {
            let (rest, first) = verify(take(1usize), |ch: &str | is_alphabetic(ch.chars().nth(0).unwrap() as u8))(o)?;
            let (rest, body) = alphanumeric0(rest)?;
            Ok((rest, Token::Ident(Ident(first.to_string() + body))))
        })
}

fn parse_token(input: &str) -> IResult<&str, Token> {
    alt((
        parse_atom,
        parse_reserved,
        parse_ident,
        map(parse_lit_int, |i| Token::Lit(Lit::Int(i))),
        map(parse_lit_str, |s| Token::Lit(Lit::Str(s))),
    ))(input)
}

fn sp(input: &str) -> IResult<&str, &str> {
    let chars = "\t\r\n ";
    take_while(move |c| chars.contains(c))(input)
}

pub fn tokenize(input: &str) -> IResult<&str, Vec<Token>> {
    many0(delimited(sp, parse_token, opt(sp)))(input)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_parse_ident() {
        let input = "abc";
        assert_eq!(Ok(("", Token::Ident(Ident("abc".to_string())))), parse_ident(input));
    }

    #[test]
    fn test_parse_with_ws() {
        let input = "let x = 3;";
        assert_eq!(Ok(("", vec![
            Token::Let,
            Token::Ident(Ident("x".to_string())),
            Token::Assign,
            Token::Lit(Lit::Int(LitInt(3))),
            Token::Semi
        ])), tokenize(input));
    }
}
use std::collections::HashMap;
use crate::parser::{Ty, Ident, LitExpr};
use crate::parser::stmt::{Stmt, LetStmt, DefStmt, RetStmt, IfStmt};
use crate::backend::Visitor;
use crate::parser::expr::{InfixExpr, CallExpr, Expr};
use crate::backend::tagged_ast::{TypedStmt, TypedAST, TypedDefStmt, TypedIfStmt};

#[derive(Debug)]
pub struct TypedASTBuilder {
    super_type_mappings: HashMap<Ident, Ty>,
    type_mappings: HashMap<Ident, Ty>,
    ast: Vec<Stmt>,
    output: Vec<TypedStmt>,
}

pub struct ExprTypeResolver<'a> {
    resolved_ty: Option<Ty>,
    ast: &'a TypedASTBuilder,
}

impl<'a> ExprTypeResolver<'a> {
    pub fn new(ast: &'a TypedASTBuilder) -> ExprTypeResolver<'a> {
        ExprTypeResolver {
            resolved_ty: None,
            ast
        }
    }
}

impl<'a> Visitor for ExprTypeResolver<'a> {
    fn visit_ident_expr(&mut self, ident: &Ident) {
        match self.ast.get_mapping(ident) {
            Some(ty) => self.resolved_ty = Some(ty.clone()),
            None => panic!("Cycle detected in type resolution. Ident `{}` has no resolved type.", ident.name)
        }
    }

    fn visit_lit_expr(&mut self, expr: &LitExpr) {
        match expr {
            LitExpr::Int(_) => self.resolved_ty = Some(Ty::Int),
            LitExpr::Str(_) => self.resolved_ty = Some(Ty::Str),
        }
    }

    fn visit_infix_expr(&mut self, _: &InfixExpr) {
        self.resolved_ty = Some(Ty::Bool);
    }

    fn visit_call_expr(&mut self, expr: &CallExpr) {
        match self.ast.get_mapping(&expr._fn) {
            Some(ty) => self.resolved_ty = Some(ty.clone()),
            None => panic!("Cycle detected in type resolution. Function `{}` has no resolved type.", expr._fn.name)
        }
    }
}

impl TypedASTBuilder {
    pub fn new(ast: Vec<Stmt>) -> TypedASTBuilder {
        TypedASTBuilder {
            super_type_mappings: HashMap::new(),
            type_mappings: HashMap::new(),
            ast,
            output: Vec::new()
        }
    }

    pub fn child_of(builder: &TypedASTBuilder, ast: Vec<Stmt>) -> TypedASTBuilder {
        TypedASTBuilder {
            super_type_mappings: builder.type_mappings.clone(),
            type_mappings: HashMap::new(),
            ast,
            output: Vec::new()
        }
    }

    pub fn get_mapping(&self, ident: &Ident) -> Option<&Ty> {
        self.type_mappings.get(ident).or_else(|| self.super_type_mappings.get(ident))
    }

    pub fn walk(mut self) -> Vec<TypedStmt> {
        for stmt in self.ast.clone().iter()  { // :(
            self.visit_stmt(stmt);
        }

        self.output
    }

    pub fn transform(mut self) -> TypedAST {
        for stmt in self.ast.clone().iter()  { // :(
            self.visit_stmt(stmt);
        }

        TypedAST { type_mappings: self.type_mappings, ast: self.output }
    }
}

impl Visitor for TypedASTBuilder {
    fn visit_expr(&mut self, expr: &Expr) {
        self.output.push(TypedStmt::Expr(expr.clone()));
    }

    fn visit_def_stmt(&mut self, stmt: &DefStmt) {
        if self.type_mappings.contains_key(&stmt.ident) || self.super_type_mappings.contains_key(&stmt.ident) {
            panic!("Shadowed function name '{}'", &stmt.ident.name);
        }

        self.type_mappings.insert(stmt.ident.clone(), stmt.ret.clone());

        let mut builder = TypedASTBuilder::child_of(&self, stmt.body.clone());
        let ast = builder.transform();
        self.output.push(TypedStmt::Def(TypedDefStmt {
            local_type_mappings: ast.type_mappings,
            ident: stmt.ident.clone(),
            args: stmt.args.clone(),
            ret: stmt.ret.clone(),
            body: ast.ast,
        }));
    }

    fn visit_let_stmt(&mut self, stmt: &LetStmt) {
        let ident = &stmt.ident;
        if self.type_mappings.contains_key(ident) {
            panic!("Shadowed variable name '{}'", ident.name);
        }

        let ty = {
            let mut resolver = ExprTypeResolver::new(&self);
            resolver.visit_expr(&stmt.val);
            resolver.resolved_ty.expect(&format!("Resolver was unable to resolve type of {:#?}", stmt))
        };

        self.type_mappings.insert(ident.clone(), ty);
        self.output.push(TypedStmt::Let(stmt.clone()));
    }

    fn visit_ret_stmt(&mut self, stmt: &RetStmt) {
        self.output.push(TypedStmt::Ret(stmt.clone()));
    }

    fn visit_if_stmt(&mut self, stmt: &IfStmt) {

        let body_builder = TypedASTBuilder::child_of(&self, stmt.body.clone());
        let body = body_builder.transform();
        let else_builder = TypedASTBuilder::child_of(&self, stmt.elif.clone());
        let elif = else_builder.transform();

        self.output.push(TypedStmt::If(TypedIfStmt {
            cond: stmt.cond.clone(),
            body_type_mappings: body.type_mappings,
            body: body.ast,
            else_type_mappings: elif.type_mappings,
            elif: elif.ast,
        }))
    }
}
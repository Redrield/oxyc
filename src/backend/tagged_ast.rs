use crate::parser::{Ty, Ident};
use std::collections::HashMap;
use crate::parser::stmt::{DefStmt, LetStmt, RetStmt, Arg};
use crate::parser::expr::Expr;

#[derive(Debug)]
pub struct TypedAST {
    pub type_mappings: HashMap<Ident, Ty>,
    pub ast: Vec<TypedStmt>
}

impl TypedAST {
    pub fn new(mappings: HashMap<Ident, Ty>, ast: Vec<TypedStmt>) -> TypedAST {
        TypedAST { type_mappings: mappings, ast }
    }
}

#[derive(Debug, PartialEq, Clone)]
pub enum TypedStmt {
    Def(TypedDefStmt),
    Let(LetStmt),
    Ret(RetStmt),
    If(TypedIfStmt),
    Expr(Expr)
}

#[derive(Debug, PartialEq, Clone)]
pub struct TypedDefStmt {
    pub local_type_mappings: HashMap<Ident, Ty>,
    pub ident: Ident,
    pub args: Vec<Arg>,
    pub ret: Ty,
    pub body: Vec<TypedStmt>
}

#[derive(Debug, PartialEq, Clone)]
pub struct TypedIfStmt {
    pub cond: Expr,
    pub body_type_mappings: HashMap<Ident, Ty>,
    pub body: Vec<TypedStmt>,
    pub else_type_mappings: HashMap<Ident, Ty>,
    pub elif: Vec<TypedStmt>
}
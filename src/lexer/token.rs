use super::lit::*;
use nom::{InputLength, InputTake, Slice, InputIter};
use std::ops::{Range, RangeTo, RangeFrom, RangeFull};
use std::iter::Enumerate;
use std::slice::Iter;

#[derive(PartialEq, Debug, Clone)]
pub enum Token {
    Assign,
    Plus,
    Minus,
    Mul,
    Div,
    Lit(Lit),
    Ident(Ident),
    LPar,
    RPar,
    Fn,
    Let,
    If,
    Else,
    Semi,
    Colon,
    Eq,
    Ne,
    Ge,
    Le,
    Gt,
    Lt,
    LBrace,
    RBrace,
    Comma,
    Return
}

#[derive(Debug, PartialEq, Clone)]
pub struct Ident(pub String);

#[derive(Debug, PartialEq, Clone, Copy)]
pub struct Tokens<'a> {
    pub tok: &'a [Token],
    start: usize,
    end: usize,
}

impl<'a> Tokens<'a> {
    pub fn new(vec: &'a Vec<Token>) -> Tokens<'a> {
        Tokens {
            tok: &vec[..],
            start: 0,
            end: vec.len(),
        }
    }
}

impl<'a> InputLength for Tokens<'a> {
    fn input_len(&self) -> usize {
        self.tok.len()
    }
}

impl<'a> InputTake for Tokens<'a> {
    fn take(&self, count: usize) -> Self {
        Tokens {
            tok: &self.tok[..count],
            start: 0,
            end: count,
        }
    }

    fn take_split(&self, count: usize) -> (Self, Self) {
        let (prefix, suffix) = self.tok.split_at(count);
        let first = Tokens {
            tok: prefix,
            start: 0,
            end: prefix.len(),
        };
        let second = Tokens {
            tok: suffix,
            start: 0,
            end: suffix.len(),
        };

        (second, first)
    }
}

impl<'a> InputLength for Token {
    fn input_len(&self) -> usize {
        1
    }
}

impl<'a> Slice<Range<usize>> for Tokens<'a> {
    fn slice(&self, range: Range<usize>) -> Self {
        Tokens {
            tok: &self.tok[range.clone()],
            start: self.start + range.start,
            end: self.end + range.end,
        }
    }
}

impl<'a> Slice<RangeTo<usize>> for Tokens<'a> {
    fn slice(&self, range: RangeTo<usize>) -> Self {
        self.slice(0..range.end)
    }
}

impl<'a> Slice<RangeFrom<usize>> for Tokens<'a> {
    fn slice(&self, range: RangeFrom<usize>) -> Self {
        self.slice(range.start..self.end - self.start)
    }
}

impl<'a> Slice<RangeFull> for Tokens<'a> {
    fn slice(&self, _: RangeFull) -> Self {
        Tokens {
            tok: self.tok,
            start: self.start,
            end: self.end,
        }
    }
}

impl<'a> InputIter for Tokens<'a> {
    type Item = &'a Token;
    type Iter = Enumerate<Iter<'a, Token>>;
    type IterElem = Iter<'a, Token>;

    fn iter_indices(&self) -> Self::Iter {
        self.tok.iter().enumerate()
    }

    fn iter_elements(&self) -> Self::IterElem {
        self.tok.iter()
    }

    fn position<P>(&self, predicate: P) -> Option<usize> where
        P: Fn(Self::Item) -> bool {
        self.tok.iter().position(|b| predicate(b))
    }

    fn slice_index(&self, count: usize) -> Option<usize> {
        if self.tok.len() >= count {
            Some(count)
        } else {
            None
        }
    }
}

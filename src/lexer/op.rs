use super::token::Token;
use nom::IResult;
use nom::bytes::complete::tag;
use nom::branch::alt;
use std::collections::hash_map::Entry::Occupied;
use nom::combinator::map;

fn parse_arith(input: &str) -> IResult<&str, Token> {
    alt((
        map(tag("+"), |_| Token::Plus),
        map(tag("-"), |_| Token::Minus),
        map(tag("*"), |_| Token::Mul),
        map(tag("/"), |_| Token::Div)
    ))(input)
}

fn parse_cmp(input: &str) -> IResult<&str, Token> {
    alt((
        map(tag(">"), |_| Token::Gt),
        map(tag("<"), |_| Token::Lt),
        map(tag("=="), |_| Token::Eq),
        map(tag("!="), |_| Token::Ne),
        map(tag(">="), |_| Token::Ge),
        map(tag("<="), |_| Token::Le)
    ))(input)
}

fn parse_assign(input: &str) -> IResult<&str, Token> {
    map(tag("="), |_| Token::Assign)(input)
}

pub fn parse_op(input: &str) -> IResult<&str, Token> {
    alt((
        parse_cmp,
        parse_assign,
        parse_arith,
    ))(input)
}

#[cfg(test)]
mod tests {
    use crate::lexer::token::*;
    use super::*;

    #[test]
    fn test_arithmetic() {
        let input = "+";
        assert_eq!(Ok(("", Token::Plus)), parse_op(input))
    }

    #[test]
    fn test_cmp() {
        let input = "!=";
        assert_eq!(Ok(("", Token::Ne)), parse_op(input));
    }

    #[test]
    fn test_assign() {
        let input = "=";

        assert_eq!(Ok(("", Token::Assign)), parse_op(input));
    }
}

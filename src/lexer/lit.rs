use nom::IResult;
use nom::sequence::delimited;
use nom::bytes::complete::{tag, escaped};
use nom::character::complete::{none_of, one_of, digit0, digit1};
use nom::combinator::{recognize, opt, map};
use std::str::FromStr;

#[derive(PartialEq, Clone, Debug)]
pub enum Lit {
    Int(LitInt),
    Str(LitStr)
}

#[derive(Debug, PartialEq, Clone)]
pub struct LitStr(pub String);

#[derive(Debug, PartialEq, Clone, Copy)]
pub struct LitInt(pub i64);

pub fn parse_lit_str(input: &str) -> IResult<&str, LitStr> {
    delimited(tag("\""),
                escaped(none_of("\\\""), '\\', one_of("\"\\")),
                tag("\""))(input)
        .map(|(remaining, res)| (remaining, LitStr(res.to_string())))
}

fn sign(input: &str) -> IResult<&str, &str> {
    recognize(opt(tag("-")))(input)
}

fn integer_lit10(input: &str) -> IResult<&str, String> {
    sign(input)
        .and_then(|(unparsed, sign)| digit1(unparsed)
            .map(|(unparsed, num)| (unparsed, sign.to_owned() + num)))
}

pub fn parse_lit_int(input: &str) -> IResult<&str, LitInt> {
    map(integer_lit10, |i| LitInt(i64::from_str(&i).unwrap()))(input)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_parse_string() {
        let input = r#""hello""#;
        assert_eq!(Ok(("", LitStr("hello".to_string()))), parse_lit_str(input));
    }

    #[test]
    fn test_parse_int() {
        let input = "10";
        assert_eq!(Ok(("", LitInt(10))), parse_lit_int(input));

        let input = "-50";
        assert_eq!(Ok(("", LitInt(-50))), parse_lit_int(input));
    }
}
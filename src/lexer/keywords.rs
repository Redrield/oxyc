use super::token::*;
use nom::IResult;
use nom::branch::alt;
use nom::bytes::complete::tag;
use nom::combinator::map;

pub fn parse_reserved(input: &str) -> IResult<&str, Token> {
    alt((
        map(tag("fn"), |_| Token::Fn),
        map(tag("let"), |_| Token::Let),
        map(tag("return"), |_| Token::Return),
        map(tag("if"), |_| Token::If),
        map(tag("else"), |_| Token::Else)
    ))(input)
}
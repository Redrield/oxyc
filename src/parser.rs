pub mod stmt;
pub mod expr;
pub mod ast;

use crate::lexer::token::{Tokens, Token};
use crate::lexer::lit::*;
use nom::IResult;
use nom::bytes::complete::take;
use nom::Err;
use nom::error::ErrorKind;
use crate::parser::stmt::{Stmt, Arg, DefStmt, LetStmt, RetStmt, IfStmt};
use nom::combinator::{opt, map};
use nom::multi::many0;
use crate::parser::expr::parse_expr;
use nom::branch::alt;

fn token(destok: Token) -> impl Fn(Tokens) -> IResult<Tokens, Token> {
    move |input: Tokens| {
        let (rest, tok) = take(1usize)(input)?;
        if destok == tok.tok[0] {
            Ok((rest, tok.tok[0].clone()))
        } else {
            Err(Err::Error((rest, ErrorKind::Tag)))
        }
    }
}

fn ident(input: Tokens) -> IResult<Tokens, String> {
    let (rest, tok) = take(1usize)(input)?;
    if let Token::Ident(i) = &tok.tok[0] {
        Ok((rest, i.0.clone()))
    } else {
        Err(Err::Error((rest, ErrorKind::Tag)))
    }
}

fn lit(input: Tokens) -> IResult<Tokens, LitExpr> {
    let (rest, tok) = take(1usize)(input)?;
    if let Token::Lit(ref lit) = &tok.tok[0] {
        let expr = match lit {
            Lit::Int(i) => LitExpr::Int(i.0),
            Lit::Str(s) => LitExpr::Str(s.0.clone()),
        };

        Ok((rest, expr))
    } else {
        Err(Err::Error((rest, ErrorKind::Tag)))
    }
}

fn ty(input: Tokens) -> IResult<Tokens, Ty> {
    let (rest, ident) = ident(input)?;
    let ty = match ident.as_str() {
        "str" => Ty::Str,
        "int" => Ty::Int,
        "bool" => Ty::Bool,
        _ => Ty::Custom(ident.clone())
    };

    Ok((rest, ty))
}

// :(
fn parse_expr_stmt(i: Tokens) -> IResult<Tokens, Stmt> {
    let (i, expr) = map(parse_expr, |e| Stmt::Expr(e))(i)?;
    let (i, _) = token(Token::Semi)(i)?;
    Ok((i, expr))
}

pub fn parse_stmt(i: Tokens) -> IResult<Tokens, Stmt> {
    alt((
        parse_let_stmt,
        parse_fn_decl,
        parse_ret_stmt,
        parse_if_stmt,
        parse_expr_stmt
    ))(i)
}

pub fn parse_stmts(i: Tokens) -> IResult<Tokens, Vec<Stmt>> {
    many0(parse_stmt)(i)
}

fn parse_if_stmt(i: Tokens) -> IResult<Tokens, Stmt> {
    let (i, _) = token(Token::If)(i)?;
    let (i, _) = token(Token::LPar)(i)?;
    let (i, cond) = parse_expr(i)?;
    let (i, _) = token(Token::RPar)(i)?;
    let (i, body) = parse_scope(i)?;
    // TODO: elif

    Ok((i, Stmt::If(IfStmt { cond, body, elif: Vec::new() })))
}

pub fn parse_fn_decl(i: Tokens) -> IResult<Tokens, Stmt> {
    let (i, _) = token(Token::Fn)(i)?;
    let (i, name) = ident(i)?;
    let (i, args) = parse_fn_args(i)?;
    let (i, ret) = parse_fn_ret(i)?;
    let (i, body) = parse_scope(i)?;
    Ok((i, Stmt::Def(DefStmt { ident: Ident { name, scope: IdentScope::Function }, args, ret: ret.unwrap_or(Ty::Unit), body })))
}

fn parse_fn_args(i: Tokens) -> IResult<Tokens, Vec<Arg>> {
    let (i, _) = token(Token::LPar)(i)?;
    let (i, arg) = opt(parse_fn_arg)(i)?;
    let (i, args) = many0(parse_comma_arg)(i)?;
    let (i, _) = token(Token::RPar)(i)?;
    if let Some(arg) = arg {
        Ok((i, [&vec![arg][..], &args[..]].concat()))
    } else {
        Ok((i, vec![]))
    }
}

fn parse_fn_ret(i: Tokens) -> IResult<Tokens, Option<Ty>> {
    let (i, tok) = opt(token(Token::Colon))(i)?;
    if let Some(_) = tok {
        ty(i).map(|(i, ty)| (i, Some(ty)))
    } else {
        Ok((i, None))
    }
}

fn parse_scope(i: Tokens) -> IResult<Tokens, Vec<Stmt>> {
    let (i, _) = token(Token::LBrace)(i)?;
    let (i, stmts) = parse_stmts(i)?;
    let (i, _) = token(Token::RBrace)(i)?;
    Ok((i, stmts))
}

fn parse_comma_arg(i: Tokens) -> IResult<Tokens, Arg> {
    let (i, _) = token(Token::Comma)(i)?;
    parse_fn_arg(i)
}

fn parse_fn_arg(i: Tokens) -> IResult<Tokens, Arg> {
    let (i, name) = ident(i)?;
    let (i, _) = token(Token::Colon)(i)?;
    let (i, ty) = ty(i)?;
    Ok((i, Arg { name: Ident { name, scope: IdentScope::Variable }, ty }))
}

fn parse_let_stmt(i: Tokens) -> IResult<Tokens, Stmt> {
    let (i, _) = token(Token::Let)(i)?;
    let (i, name) = ident(i)?;
    let (i, _) = token(Token::Assign)(i)?;
    let (i, val) = parse_expr(i)?;
    let (i, _) = token(Token::Semi)(i)?;
    Ok((i, Stmt::Let(LetStmt { ident: Ident { name, scope: IdentScope::Variable }, val })))
}

fn parse_ret_stmt(i: Tokens) -> IResult<Tokens, Stmt> {
    let (i, _) = token(Token::Return)(i)?;
    let (i, val) = parse_expr(i)?;
    let (i, _) = token(Token::Semi)(i)?;
    Ok((i, Stmt::Ret(RetStmt { val })))
}

#[derive(Debug, PartialEq, Clone, Hash, Eq)]
pub struct Ident {
    pub name: String,
    pub scope: IdentScope
}

#[derive(Debug, PartialEq, Clone, Hash, Eq)]
pub enum IdentScope {
    Function,
    Variable,
}

#[derive(Debug, PartialEq, Clone)]
pub enum LitExpr {
    Int(i64),
    Str(String),
}

#[derive(Debug, PartialEq, Clone)]
pub enum Ty {
    Str,
    Int,
    Unit,
    Bool,
    Custom(String),
}

#[cfg(test)]
mod tests {
    use crate::lexer::token::{Tokens, Token, Ident};
    use crate::lexer::lit::{Lit, LitInt, LitStr};
    use crate::parser::stmt::{Stmt, LetStmt, IfStmt};
    use crate::parser::{Ident as PIdent, LitExpr, IdentScope};
    use crate::parser::expr::{Expr, CallExpr, InfixExpr};
    use crate::parser::ast::InfixOp;

    #[test]
    fn test_let_stmt() {
        let tokens = vec![Token::Let, Token::Ident(Ident("x".to_string())), Token::Assign, Token::Lit(Lit::Int(LitInt(3))), Token::Semi];
        let tokenized = Tokens::new(&tokens);
        let (_, parsed) = super::parse_stmts(tokenized).unwrap();
        assert_eq!(parsed, vec![Stmt::Let(LetStmt { ident: PIdent { name: "x".to_string(), scope: IdentScope::Variable }, val: Expr::Lit(LitExpr::Int(3)) })])
    }

    #[test]
    fn test_expr_stmt() {
        let tokens = vec![Token::Ident(Ident("func".to_string())), Token::LPar, Token::Lit(Lit::Int(LitInt(3))), Token::Comma, Token::Ident(Ident("x".to_string())), Token::RPar, Token::Semi];
        let tokenized = Tokens::new(&tokens);
        let (_, parsed) = super::parse_stmts(tokenized).unwrap();
        assert_eq!(parsed,
                   vec![
                       Stmt::Expr(Expr::Call(CallExpr {
                           _fn: PIdent { name: "func".to_string(), scope: IdentScope::Function },
                           args: vec![
                               Expr::Lit(LitExpr::Int(3)),
                               Expr::Ident(PIdent { name: "x".to_string(), scope: IdentScope::Variable })
                           ],
                       }))
                   ]);
    }

    #[test]
    fn test_if_stmt() {
        let tokens = vec![Token::If, Token::LPar, Token::Ident(Ident("x".to_string())), Token::Eq, Token::Lit(Lit::Int(LitInt(2))), Token::RPar, Token::LBrace,
                          Token::Ident(Ident("func".to_string())), Token::LPar, Token::RPar, Token::Semi,
                          Token::RBrace];
        let tokenized = Tokens::new(&tokens);
        let (_, parsed) = super::parse_stmts(tokenized).unwrap();

        assert_eq!(parsed,
                   vec![
                       Stmt::If(IfStmt {
                           cond: Expr::Infix(InfixExpr { left: Box::new(Expr::Ident(PIdent { name: "x".to_string(), scope: IdentScope::Variable })), op: InfixOp::Eq, right: Box::new(Expr::Lit(LitExpr::Int(2))) }),
                           body: vec![
                               Stmt::Expr(Expr::Call(CallExpr {
                                   _fn: PIdent { name: "func".to_string(), scope: IdentScope::Function },
                                   args: Vec::new(),
                               }))
                           ],
                           elif: vec![],
                       })
                   ]);
    }

    #[test]
    fn test_multi() {
        let tokens = vec![Token::Let, Token::Ident(Ident("x".to_string())), Token::Assign, Token::Lit(Lit::Str(LitStr("Hello!".to_string()))), Token::Semi,
                          Token::If, Token::LPar, Token::Ident(Ident("x".to_string())), Token::Eq, Token::Lit(Lit::Str(LitStr("Hello!".to_string()))), Token::RPar, Token::LBrace,
                          Token::Ident(Ident("println".to_string())), Token::LPar, Token::Ident(Ident("x".to_string())), Token::RPar, Token::Semi,
                          Token::RBrace];
        let tokenized = Tokens::new(&tokens);

        let (_, parsed) = super::parse_stmts(tokenized).unwrap();

        assert_eq!(parsed,
                   vec![
                       Stmt::Let(LetStmt {
                           ident: PIdent { name: "x".to_string(), scope: IdentScope::Variable },
                           val: Expr::Lit(LitExpr::Str("Hello!".to_string())),
                       }),
                       Stmt::If(IfStmt {
                           cond: Expr::Infix(InfixExpr {
                               left: Box::new(Expr::Ident(PIdent { name: "x".to_string(), scope: IdentScope::Variable })),
                               op: InfixOp::Eq,
                               right: Box::new(Expr::Lit(LitExpr::Str("Hello!".to_string()))),
                           }),
                           body: vec![
                               Stmt::Expr(Expr::Call(CallExpr {
                                   _fn: PIdent { name: "println".to_string(), scope: IdentScope::Function },
                                   args: vec![Expr::Ident(PIdent { name: "x".to_string(), scope: IdentScope::Variable })],
                               }))
                           ],
                           elif: Vec::new(),
                       })
                   ])
    }

    #[test]
    fn test_frontend() {
        let code = r#"
        let x = 3;
        if(x == 3) {
            println(x);
        }
        "#;

        let (remaining, tokens) = crate::lexer::tokenize(code).unwrap();
        let tokens = Tokens::new(&tokens);
        let (_, parsed) = super::parse_stmts(tokens).unwrap();

        assert_eq!(parsed,
                   vec![
                        Stmt::Let(LetStmt {
                            ident: PIdent { name: "x".to_string(), scope: IdentScope::Variable },
                            val: Expr::Lit(LitExpr::Int(3))
                        }),
                       Stmt::If(IfStmt {
                           cond: Expr::Infix(InfixExpr {
                               left: Box::new(Expr::Ident(PIdent { name: "x".to_string(), scope: IdentScope::Variable })),
                               op: InfixOp::Eq,
                               right: Box::new(Expr::Lit(LitExpr::Int(3))),
                           }),
                           body: vec![
                               Stmt::Expr(Expr::Call(CallExpr {
                                   _fn: PIdent { name: "println".to_string(), scope: IdentScope::Function },
                                   args: vec![Expr::Ident(PIdent { name: "x".to_string(), scope: IdentScope::Variable })]
                               }))
                           ],
                           elif: vec![]
                       })
                   ]);
    }
}

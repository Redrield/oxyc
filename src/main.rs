use crate::lexer::token::Tokens;
use crate::backend::tag::TypedASTBuilder;

mod lexer;
mod parser;
mod backend;

fn main() {
    let code = r#"
    let x = 3;
    fn test(): str {
        return "Foo";
    }

    if(x == 3) {
        let y = test();
    }
    "#;
    let (_, tokens) = lexer::tokenize(code).unwrap();
    let tokens = Tokens::new(&tokens);
    let (_, ast) = parser::parse_stmts(tokens).unwrap();
    let mut ast = TypedASTBuilder::new(ast).transform();

    println!("Final AST with type mappings: {:#?}", ast);
}

use llvm_sys::transforms::pass_manager_builder::*;
use llvm_sys::transforms::ipo::*;
use llvm_sys::prelude::*;
use llvm_sys::core::*;

use super::Context;

pub struct PassManager {
    inner: LLVMPassManagerRef,
}

pub struct PassManagerBuilder {
    inner: LLVMPassManagerBuilderRef
}

pub enum PassType {
    GlobalDCE,
    StripSymbols,
    AlwaysInliner,
    ArgumentPromotion,
    ConstantMerge,
    DeadArgElimination,
    FunctionAttrs,
    FunctionInlining,
    GlobalOptimizer,
    IPConstantPropogation,
    IPSCCP,
    PruneEH,
    StripDeadPrototypes,
}

impl Drop for PassManagerBuilder {
    fn drop(&mut self) {
        unsafe {
            LLVMPassManagerBuilderDispose(self.inner)
        }
    }
}

impl Drop for PassManager {
    fn drop(&mut self) {
        unsafe {
            LLVMDisposePassManager(self.inner)
        }
    }
}

impl PassManagerBuilder {
    pub fn new() -> PassManagerBuilder {
        let inner = unsafe {
            LLVMPassManagerBuilderCreate()
        };

        PassManagerBuilder {
            inner
        }
    }

    pub fn set_opt_level(&self, opt_level: u32) {
        if opt_level <= 3 {
            unsafe {
                LLVMPassManagerBuilderSetOptLevel(self.inner, opt_level)
            }
        }
    }

    pub fn set_disable_simplify_lib_calls(&self, disable: bool) {
        unsafe {
            LLVMPassManagerBuilderSetDisableSimplifyLibCalls(self.inner, disable as LLVMBool)
        }
    }

    pub fn set_disable_unit_at_a_time(&self, disable: bool) {
        unsafe {
            LLVMPassManagerBuilderSetDisableUnitAtATime(self.inner, disable as LLVMBool)
        }
    }

    pub fn set_disable_unroll_loops(&self, disable: bool) {
        unsafe {
            LLVMPassManagerBuilderSetDisableUnrollLoops(self.inner, disable as LLVMBool)
        }
    }

    pub fn set_size_level(&self, size: u32) {
        if size <= 2 {
            unsafe {
                LLVMPassManagerBuilderSetSizeLevel(self.inner, size)
            }
        }
    }

    pub fn build(&mut self) -> PassManager {
        let inner = unsafe {
            LLVMCreatePassManager()
        };

        unsafe {
            LLVMPassManagerBuilderPopulateModulePassManager(self.inner, inner);
        }

        PassManager {
            inner
        }
    }
}

impl PassManager {
    pub fn add_pass(&self, pass: PassType) {
        unsafe {
            match pass {
                PassType::GlobalDCE => LLVMAddGlobalDCEPass(self.inner),
                PassType::StripSymbols => LLVMAddStripSymbolsPass(self.inner),
                PassType::AlwaysInliner => LLVMAddAlwaysInlinerPass(self.inner),
                PassType::ArgumentPromotion => LLVMAddArgumentPromotionPass(self.inner),
                PassType::ConstantMerge => LLVMAddConstantMergePass(self.inner),
                PassType::DeadArgElimination => LLVMAddDeadArgEliminationPass(self.inner),
                PassType::FunctionAttrs => LLVMAddFunctionAttrsPass(self.inner),
                PassType::FunctionInlining => LLVMAddFunctionInliningPass(self.inner),
                PassType::GlobalOptimizer => LLVMAddGlobalOptimizerPass(self.inner),
                PassType::IPConstantPropogation => LLVMAddIPConstantPropagationPass(self.inner),
                PassType::IPSCCP => LLVMAddIPSCCPPass(self.inner),
                PassType::PruneEH => LLVMAddPruneEHPass(self.inner),
                PassType::StripDeadPrototypes => LLVMAddStripDeadPrototypesPass(self.inner),
            }
        }
    }

    pub fn run<'a>(&self, module: &'a Context) {
        unsafe {
            LLVMRunPassManager(self.inner, module.llmod);
        }
    }
}

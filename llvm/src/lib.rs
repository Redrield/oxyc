pub extern crate llvm_sys;
extern crate libc;

pub mod context;
pub mod builder;
pub mod type_;
pub mod func;
pub mod codegen;
pub mod transform;
pub mod struct_;

use context::*;
use type_::*;
use func::Function;

use llvm_sys::prelude::*;
use llvm_sys::core::*;

use std::ffi::CString;

/// Declares a new function with name [name] and type [fn_type]  in the given [Context].
pub fn declare_fn(cx: &Context, name: &str, fn_type: FunctionType) -> Function {
    let cname = CString::new(name).unwrap();

    let llfn = unsafe {
        LLVMAddFunction(cx.llmod, cname.as_ptr(), fn_type.to_ref())
    };

    Function::new(fn_type, llfn)
}

/// Declares a new global with name [name] and type [ty] in the given [cx]
pub fn global(cx: &Context, name: &str, ty: Type) -> LLVMValueRef {
    let cname = CString::new(name).unwrap();

    unsafe {
        LLVMAddGlobal(cx.llmod, *ty, cname.as_ptr())
    }
}
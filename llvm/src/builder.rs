use libc;

use llvm_sys::prelude::*;
use llvm_sys::core::*;
use llvm_sys::{LLVMOpcode, LLVMIntPredicate, LLVMRealPredicate};

use std::ffi::CString;

use context::Context;
use type_::*;

/// Wrapper struct around LLVM builder functions
/// Shamelessly taken from librustc_trans.
pub struct Builder<'a> {
    pub llbuilder: LLVMBuilderRef,
    pub llcx: &'a Context,
}

pub(crate) const TRUE: LLVMBool = 1 as LLVMBool;
pub(crate) const FALSE: LLVMBool = 0 as LLVMBool;

impl<'a> Drop for Builder<'a> {
    fn drop(&mut self) {
        unsafe {
            LLVMDisposeBuilder(self.llbuilder);
        }
    }
}

fn noname() -> *const libc::c_char {
    static CNULL: libc::c_char = 0;
    &CNULL
}

impl<'a> Builder<'a> {
    pub fn new_block(ctx: &'a Context, llfn: LLVMValueRef) -> Builder<'a> {
        let bx = Builder::new(ctx);
        let block = unsafe {
            LLVMAppendBasicBlockInContext(ctx.llcx, llfn, noname())
        };

        bx.position_at_end(block);
        bx
    }

    fn new(ctx: &'a Context) -> Builder<'a> {
        let llbuilder = unsafe {
            LLVMCreateBuilderInContext(ctx.llcx)
        };

        Builder {
            llbuilder,
            llcx: ctx,
        }
    }

    pub fn build_sibling_block(&self) -> Builder<'a> {
        Builder::new_block(self.llcx, self.llfn())
    }

    pub fn create_block(&self) -> LLVMBasicBlockRef {
        unsafe {
            LLVMAppendBasicBlockInContext(self.llcx.llcx, self.llfn(), noname())
        }
    }

    pub fn set_value_name(&self, value: LLVMValueRef, name: &str) {
        let cname = CString::new(name).unwrap();
        unsafe {
            LLVMSetValueName2(value, cname.as_ptr(), name.len());
        }
    }

    pub fn position_at_end(&self, llbb: LLVMBasicBlockRef) {
        unsafe {
            LLVMPositionBuilderAtEnd(self.llbuilder, llbb);
        }
    }

    pub fn llfn(&self) -> LLVMValueRef {
        unsafe {
            LLVMGetBasicBlockParent(self.llbb())
        }
    }

    pub fn llbb(&self) -> LLVMBasicBlockRef {
        unsafe {
            LLVMGetInsertBlock(self.llbuilder)
        }
    }

    pub fn ret_void(&self) {
        unsafe {
            LLVMBuildRetVoid(self.llbuilder);
        }
    }

    pub fn ret(&self, v: LLVMValueRef) {
        unsafe {
            LLVMBuildRet(self.llbuilder, v);
        }
    }

    pub fn aggregate_ret(&self, ret_vals: &[LLVMValueRef]) {
        unsafe {
            LLVMBuildAggregateRet(self.llbuilder,
                                  ret_vals.as_ptr() as *mut LLVMValueRef,
                                  ret_vals.len() as libc::c_uint);
        }
    }

    pub fn cond_br(&self, v: LLVMValueRef, then: LLVMBasicBlockRef, else_llbb: LLVMBasicBlockRef) -> LLVMValueRef {
        unsafe {
            LLVMBuildCondBr(self.llbuilder, v, then, else_llbb)
        }
    }

    pub fn br(&self, br: LLVMBasicBlockRef) -> LLVMValueRef {
        unsafe {
            LLVMBuildBr(self.llbuilder, br)
        }
    }

    pub fn switch(&self, v: LLVMValueRef, else_llbb: LLVMBasicBlockRef, num_cases: usize) -> LLVMValueRef {
        unsafe {
            LLVMBuildSwitch(self.llbuilder, v, else_llbb, num_cases as libc::c_uint)
        }
    }

    pub fn indirect_br(&self, addr: LLVMValueRef, num_dests: usize) {
        unsafe {
            LLVMBuildIndirectBr(self.llbuilder, addr, num_dests as libc::c_uint);
        }
    }

    pub fn unreachable(&self) {
        unsafe {
            LLVMBuildUnreachable(self.llbuilder);
        }
    }

    pub fn add(&self, lhs: LLVMValueRef, rhs: LLVMValueRef) -> LLVMValueRef {
        unsafe {
            LLVMBuildAdd(self.llbuilder, lhs, rhs, noname())
        }
    }

    pub fn nswadd(&self, lhs: LLVMValueRef, rhs: LLVMValueRef) -> LLVMValueRef {
        unsafe {
            LLVMBuildNSWAdd(self.llbuilder, lhs, rhs, noname())
        }
    }

    pub fn nuwadd(&self, lhs: LLVMValueRef, rhs: LLVMValueRef) -> LLVMValueRef {
        unsafe {
            LLVMBuildNUWAdd(self.llbuilder, lhs, rhs, noname())
        }
    }

    pub fn fadd(&self, lhs: LLVMValueRef, rhs: LLVMValueRef) -> LLVMValueRef {
        unsafe {
            LLVMBuildFAdd(self.llbuilder, lhs, rhs, noname())
        }
    }

    pub fn sub(&self, lhs: LLVMValueRef, rhs: LLVMValueRef) -> LLVMValueRef {
        unsafe {
            LLVMBuildSub(self.llbuilder, lhs, rhs, noname())
        }
    }

    pub fn nswsub(&self, lhs: LLVMValueRef, rhs: LLVMValueRef) -> LLVMValueRef {
        unsafe {
            LLVMBuildNSWSub(self.llbuilder, lhs, rhs, noname())
        }
    }

    pub fn nuwsub(&self, lhs: LLVMValueRef, rhs: LLVMValueRef) -> LLVMValueRef {
        unsafe {
            LLVMBuildNUWSub(self.llbuilder, lhs, rhs, noname())
        }
    }

    pub fn fsub(&self, lhs: LLVMValueRef, rhs: LLVMValueRef) -> LLVMValueRef {
        unsafe {
            LLVMBuildFSub(self.llbuilder, lhs, rhs, noname())
        }
    }

    pub fn mul(&self, lhs: LLVMValueRef, rhs: LLVMValueRef) -> LLVMValueRef {
        unsafe {
            LLVMBuildMul(self.llbuilder, lhs, rhs, noname())
        }
    }

    pub fn nuwmul(&self, lhs: LLVMValueRef, rhs: LLVMValueRef) -> LLVMValueRef {
        unsafe {
            LLVMBuildNUWMul(self.llbuilder, lhs, rhs, noname())
        }
    }

    pub fn fmul(&self, lhs: LLVMValueRef, rhs: LLVMValueRef) -> LLVMValueRef {
        unsafe {
            LLVMBuildFMul(self.llbuilder, lhs, rhs, noname())
        }
    }

    pub fn udiv(&self, lhs: LLVMValueRef, rhs: LLVMValueRef) -> LLVMValueRef {
        unsafe {
            LLVMBuildUDiv(self.llbuilder, lhs, rhs, noname())
        }
    }

    pub fn sdiv(&self, lhs: LLVMValueRef, rhs: LLVMValueRef) -> LLVMValueRef {
        unsafe {
            LLVMBuildSDiv(self.llbuilder, lhs, rhs, noname())
        }
    }

    pub fn exactsdiv(&self, lhs: LLVMValueRef, rhs: LLVMValueRef) -> LLVMValueRef {
        unsafe {
            LLVMBuildExactSDiv(self.llbuilder, lhs, rhs, noname())
        }
    }

    pub fn fdiv(&self, lhs: LLVMValueRef, rhs: LLVMValueRef) -> LLVMValueRef {
        unsafe {
            LLVMBuildFDiv(self.llbuilder, lhs, rhs, noname())
        }
    }

    pub fn urem(&self, lhs: LLVMValueRef, rhs: LLVMValueRef) -> LLVMValueRef {
        unsafe {
            LLVMBuildUDiv(self.llbuilder, lhs, rhs, noname())
        }
    }

    pub fn srem(&self, lhs: LLVMValueRef, rhs: LLVMValueRef) -> LLVMValueRef {
        unsafe {
            LLVMBuildSRem(self.llbuilder, lhs, rhs, noname())
        }
    }

    pub fn frem(&self, lhs: LLVMValueRef, rhs: LLVMValueRef) -> LLVMValueRef {
        unsafe {
            LLVMBuildFRem(self.llbuilder, lhs, rhs, noname())
        }
    }

    pub fn shl(&self, lhs: LLVMValueRef, rhs: LLVMValueRef) -> LLVMValueRef {
        unsafe {
            LLVMBuildShl(self.llbuilder, lhs, rhs, noname())
        }
    }

    pub fn lshr(&self, lhs: LLVMValueRef, rhs: LLVMValueRef) -> LLVMValueRef {
        unsafe {
            LLVMBuildLShr(self.llbuilder, lhs, rhs, noname())
        }
    }

    pub fn ashr(&self, lhs: LLVMValueRef, rhs: LLVMValueRef) -> LLVMValueRef {
        unsafe {
            LLVMBuildAShr(self.llbuilder, lhs, rhs, noname())
        }
    }

    pub fn and(&self, lhs: LLVMValueRef, rhs: LLVMValueRef) -> LLVMValueRef {
        unsafe {
            LLVMBuildAnd(self.llbuilder, lhs, rhs, noname())
        }
    }

    pub fn or(&self, lhs: LLVMValueRef, rhs: LLVMValueRef) -> LLVMValueRef {
        unsafe {
            LLVMBuildOr(self.llbuilder, lhs, rhs, noname())
        }
    }

    pub fn xor(&self, lhs: LLVMValueRef, rhs: LLVMValueRef) -> LLVMValueRef {
        unsafe {
            LLVMBuildXor(self.llbuilder, lhs, rhs, noname())
        }
    }

    pub fn binop(&self, op: LLVMOpcode, lhs: LLVMValueRef, rhs: LLVMValueRef) -> LLVMValueRef {
        unsafe {
            LLVMBuildBinOp(self.llbuilder, op, lhs, rhs, noname())
        }
    }

    pub fn neg(&self, v: LLVMValueRef) -> LLVMValueRef {
        unsafe {
            LLVMBuildNeg(self.llbuilder, v, noname())
        }
    }

    pub fn nswneg(&self, v: LLVMValueRef) -> LLVMValueRef {
        unsafe {
            LLVMBuildNSWNeg(self.llbuilder, v, noname())
        }
    }

    pub fn nuwneg(&self, v: LLVMValueRef) -> LLVMValueRef {
        unsafe {
            LLVMBuildNUWNeg(self.llbuilder, v, noname())
        }
    }

    pub fn fneg(&self, v: LLVMValueRef) -> LLVMValueRef {
        unsafe {
            LLVMBuildFNeg(self.llbuilder, v, noname())
        }
    }

    pub fn not(&self, v: LLVMValueRef) -> LLVMValueRef {
        unsafe {
            LLVMBuildNot(self.llbuilder, v, noname())
        }
    }

    pub fn free(&self, ptr: LLVMValueRef) {
        unsafe {
            LLVMBuildFree(self.llbuilder, ptr);
        }
    }

    pub fn load(&self, ptr: LLVMValueRef) -> LLVMValueRef {
        unsafe {
            let load = LLVMBuildLoad(self.llbuilder, ptr, noname());

//            LLVMSetAlignment(load, align.abi as libc::c_uint);
            load
        }
    }

    pub fn volatile_load(&self, ptr: LLVMValueRef) -> LLVMValueRef {
        unsafe {
            let insn = LLVMBuildLoad(self.llbuilder, ptr, noname());
            LLVMSetVolatile(insn, TRUE);
            insn
        }
    }

    pub fn store(&self, val: LLVMValueRef, ptr: LLVMValueRef) -> LLVMValueRef {
        //FIXME: rustc does validation on the pointer, see if I should do that
        unsafe {
            let store = LLVMBuildStore(self.llbuilder, val, ptr);
//            LLVMSetAlignment(store, align.abi as libc::c_uint);
            store
        }
    }

    pub fn volatile_store(&self, val: LLVMValueRef, ptr: LLVMValueRef) -> LLVMValueRef {
        unsafe {
            let store = LLVMBuildStore(self.llbuilder, val, ptr);
            LLVMSetVolatile(store, TRUE);
            store
        }
    }

    pub fn gep(&self, ptr: LLVMValueRef, indices: &[LLVMValueRef]) -> LLVMValueRef {
        unsafe {
            LLVMBuildGEP(self.llbuilder, ptr, indices.as_ptr() as *mut LLVMValueRef,
                         indices.len() as libc::c_uint, noname())
        }
    }

    pub fn inbounds_gep(&self, ptr: LLVMValueRef, indices: &[LLVMValueRef]) -> LLVMValueRef {
        unsafe {
            LLVMBuildInBoundsGEP(self.llbuilder, ptr, indices.as_ptr() as *mut LLVMValueRef,
                                 indices.len() as libc::c_uint, noname())
        }
    }

    pub fn struct_gep(&self, ptr: LLVMValueRef, idx: u64) -> LLVMValueRef {
        assert_eq!(idx as libc::c_uint as u64, idx);

        unsafe {
            LLVMBuildStructGEP(self.llbuilder, ptr, idx as libc::c_uint, noname())
        }
    }

    pub fn global_string(&self, _str: &str) -> LLVMValueRef {
        let value = CString::new(_str).unwrap();

        unsafe {
            LLVMBuildGlobalString(self.llbuilder, value.as_ptr(), noname())
        }
    }

    pub fn global_string_ptr(&self, _str: &str) -> LLVMValueRef {
        let value = CString::new(_str).unwrap();

        unsafe {
            LLVMBuildGlobalStringPtr(self.llbuilder, value.as_ptr(), noname())
        }
    }

    pub fn trunc(&self, val: LLVMValueRef, ty: Type) -> LLVMValueRef {
        unsafe {
            LLVMBuildTrunc(self.llbuilder, val, *ty, noname())
        }
    }

    pub fn zext(&self, val: LLVMValueRef, ty: Type) -> LLVMValueRef {
        unsafe {
            LLVMBuildZExt(self.llbuilder, val, *ty, noname())
        }
    }

    pub fn sext(&self, val: LLVMValueRef, ty: Type) -> LLVMValueRef {
        unsafe {
            LLVMBuildSExt(self.llbuilder, val, *ty, noname())
        }
    }

    pub fn fptoui(&self, val: LLVMValueRef, ty: Type) -> LLVMValueRef {
        unsafe {
            LLVMBuildFPToUI(self.llbuilder, val, *ty, noname())
        }
    }

    pub fn fptosi(&self, val: LLVMValueRef, ty: Type) -> LLVMValueRef {
        unsafe {
            LLVMBuildFPToSI(self.llbuilder, val, *ty, noname())
        }
    }

    pub fn uitofp(&self, val: LLVMValueRef, ty: Type) -> LLVMValueRef {
        unsafe {
            LLVMBuildUIToFP(self.llbuilder, val, *ty, noname())
        }
    }

    pub fn sitofp(&self, val: LLVMValueRef, ty: Type) -> LLVMValueRef {
        unsafe {
            LLVMBuildSIToFP(self.llbuilder, val, *ty, noname())
        }
    }

    pub fn fptrunc(&self, val: LLVMValueRef, ty: Type) -> LLVMValueRef {
        unsafe {
            LLVMBuildFPTrunc(self.llbuilder, val, *ty, noname())
        }
    }

    pub fn fpext(&self, val: LLVMValueRef, ty: Type) -> LLVMValueRef {
        unsafe {
            LLVMBuildFPExt(self.llbuilder, val, *ty, noname())
        }
    }

    pub fn ptrtoint(&self, val: LLVMValueRef, ty: Type) -> LLVMValueRef {
        unsafe {
            LLVMBuildPtrToInt(self.llbuilder, val, *ty, noname())
        }
    }

    pub fn inttoptr(&self, val: LLVMValueRef, ty: Type) -> LLVMValueRef {
        unsafe {
            LLVMBuildIntToPtr(self.llbuilder, val, *ty, noname())
        }
    }

    pub fn bitcast(&self, val: LLVMValueRef, ty: Type) -> LLVMValueRef {
        unsafe {
            LLVMBuildBitCast(self.llbuilder, val, *ty, noname())
        }
    }

    pub fn zext_or_bitcast(&self, val: LLVMValueRef, ty: Type) -> LLVMValueRef {
        unsafe {
            LLVMBuildZExtOrBitCast(self.llbuilder, val, *ty, noname())
        }
    }

    pub fn sext_or_bitcast(&self, val: LLVMValueRef, ty: Type) -> LLVMValueRef {
        unsafe {
            LLVMBuildSExtOrBitCast(self.llbuilder, val, *ty, noname())
        }
    }

    pub fn trunc_or_bitcast(&self, val: LLVMValueRef, ty: Type) -> LLVMValueRef {
        unsafe {
            LLVMBuildTruncOrBitCast(self.llbuilder, val, *ty, noname())
        }
    }

    pub fn cast(&self, op: LLVMOpcode, val: LLVMValueRef, ty: Type) -> LLVMValueRef {
        unsafe {
            LLVMBuildCast(self.llbuilder, op, val, *ty, noname())
        }
    }

    pub fn pointercast(&self, val: LLVMValueRef, ty: Type) -> LLVMValueRef {
        unsafe {
            LLVMBuildPointerCast(self.llbuilder, val, *ty, noname())
        }
    }

    pub fn intcast(&self, val: LLVMValueRef, ty: Type) -> LLVMValueRef {
        unsafe {
            LLVMBuildIntCast(self.llbuilder, val, *ty, noname())
        }
    }

    pub fn fpcast(&self, val: LLVMValueRef, ty: Type) -> LLVMValueRef {
        unsafe {
            LLVMBuildFPCast(self.llbuilder, val, *ty, noname())
        }
    }

    pub fn icmp(&self, op: LLVMIntPredicate, lhs: LLVMValueRef, rhs: LLVMValueRef) -> LLVMValueRef {
        unsafe {
            LLVMBuildICmp(self.llbuilder, op, lhs, rhs, noname())
        }
    }

    pub fn fcmp(&self, op: LLVMRealPredicate, lhs: LLVMValueRef, rhs: LLVMValueRef) -> LLVMValueRef {
        unsafe {
            LLVMBuildFCmp(self.llbuilder, op, lhs, rhs, noname())
        }
    }

    pub fn call(&self, fn_: LLVMValueRef, args: &[LLVMValueRef], tail_call: bool) -> LLVMValueRef {
        let value = unsafe {
            LLVMBuildCall(self.llbuilder, fn_, args.as_ptr() as *mut LLVMValueRef, args.len() as libc::c_uint,
                          noname())
        };

        if tail_call {
            unsafe {
                LLVMSetTailCall(value, TRUE);
            }
        }

        value
    }

    pub fn alloca(&self, t: Type) -> LLVMValueRef {
        unsafe {
            LLVMBuildAlloca(self.llbuilder, *t, noname())
        }
    }
}

use llvm_sys::prelude::*;
use llvm_sys::core::*;
use llvm_sys::analysis::*;

use std::ffi::{CStr, CString};
use std::ptr;

use std::fmt::{self, Debug};

/// Safe wrapper around LLVM Context. Taken from librustc_trans with a bit extra for debugging purposes
#[derive(Clone)]
pub struct Context {
    pub llcx: LLVMContextRef,
    pub llmod: LLVMModuleRef,
}

impl Debug for Context {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        f.write_str("Context")
    }
}

impl Context {
    /// Creates a new context and module with the given id
    pub fn new(mod_id: &str) -> Context {
        let ctx = unsafe {
            LLVMContextCreate()
        };

        let llmod = unsafe {
            let mod_name = CString::new(mod_id).unwrap();
            LLVMModuleCreateWithNameInContext(mod_name.as_ptr(), ctx)
        };

        Context {
            llcx: ctx,
            llmod,
        }
    }

    /// Gets the current LLVM IR representation of `self.llmod`
    pub fn get_ir(&self) -> String {
        let str_ptr = unsafe {
            let ptr = LLVMPrintModuleToString(self.llmod);

            let string = CStr::from_ptr(ptr);

            LLVMDisposeMessage(ptr);
            string.to_string_lossy().into_owned()
        };

        str_ptr
    }

    /// Wrapper around `LLVMVerifyModule`. Used to verify the validity of code in `self.llmod`
    pub fn verify(&self) {
        unsafe {
            let mut error = ptr::null_mut();

            LLVMVerifyModule(self.llmod, LLVMVerifierFailureAction::LLVMAbortProcessAction, &mut error);
        }
    }
}

impl Drop for Context {
    fn drop(&mut self) {
        unsafe {
            LLVMDisposeModule(self.llmod);
            LLVMContextDispose(self.llcx);
        }
    }
}
use llvm_sys::prelude::*;
use llvm_sys::core::*;

use std::ops::Deref;

use super::type_::FunctionType;

pub use llvm_sys::LLVMLinkage as Linkage;

/// Struct representing a function value in LLVM
#[derive(Clone)]
pub struct Function {
    ty: FunctionType,
    rf: LLVMValueRef
}

use std::fmt::{self, Debug};

impl Debug for Function {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let msg = unsafe {
            use std::ffi::CStr;
            let ptr = LLVMPrintValueToString(self.rf);
            let cstr = CStr::from_ptr(ptr).to_string_lossy().into_owned();
            LLVMDisposeMessage(ptr);

            cstr
        };

        f.write_str(&msg)
    }
}

impl Function {
    /// Creates a new function. Only used directly from `llvm::define_fn`
    pub(crate) fn new(ty: FunctionType, rf: LLVMValueRef) -> Function {
        Function {
            ty,
            rf
        }
    }

    /// Gets the function type of this function
    pub fn function_type(&self) -> &FunctionType {
        &self.ty
    }

    /// Sets the linkage of this function to [linkage]
    pub fn set_linkage(&self, linkage: Linkage) {
        unsafe {
            LLVMSetLinkage(self.rf, linkage)
        }
    }

    /// Returns the inner reference to this function
    #[deprecated(note = "Use std::ops::Deref implementation instead")]
    pub fn to_ref(&self) -> LLVMValueRef {
        self.rf
    }
}

impl Deref for Function {
    type Target = LLVMValueRef;

    fn deref(&self) -> &Self::Target {
        &self.rf
    }

}
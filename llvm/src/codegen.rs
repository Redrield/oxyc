use context::Context;

use llvm_sys::target::*;
use llvm_sys::target_machine::*;
use llvm_sys::core::LLVMDisposeMessage;

use libc;

use std::ptr;
use std::ffi::{CStr, CString};
use std::fs;
use std::process::Command;

/// Safe wrapper over LLVM code generation APIs
/// Takes a `Context` reference, and uses environment data
/// to compile the context
pub struct CodeGenerator<'a> {
    cx: &'a Context, // The context we want to compile
    target_machine: LLVMTargetMachineRef,
}

fn empty_str() -> *const libc::c_char {
    let value: libc::c_char = 0;

    &value
}

impl<'a> Drop for CodeGenerator<'a> {
    fn drop(&mut self) {
        unsafe {
            LLVMDisposeTargetMachine(self.target_machine)
        }
    }
}

impl<'a> CodeGenerator<'a> {
    /// Creates a new CodeGenerator for the given `Context`
    pub fn new(cx: &'a Context) -> CodeGenerator {
        unsafe {
            LLVM_InitializeNativeTarget();
            LLVM_InitializeAllTargetMCs();
            LLVM_InitializeNativeAsmPrinter();
            LLVM_InitializeNativeAsmParser();
        }

        let target = unsafe {
            LLVMGetFirstTarget()
        };
        let cpu_str = CString::new("skylake").unwrap();
        let target_triple = unsafe {
            let ptr = LLVMGetDefaultTargetTriple();
            let s = CStr::from_ptr(ptr);
            LLVMDisposeMessage(ptr);

            s.to_string_lossy().into_owned()
        };

        let reloc_mode = LLVMRelocMode::LLVMRelocPIC;
        let opt_level = LLVMCodeGenOptLevel::LLVMCodeGenLevelDefault;
        let code_model = LLVMCodeModel::LLVMCodeModelDefault;

        let target_machine = unsafe {
            LLVMCreateTargetMachine(target, target_triple.as_ptr() as *const i8, cpu_str.as_ptr(), empty_str(),
                                    opt_level, reloc_mode, code_model)
        };

        CodeGenerator {
            cx,
            target_machine
        }
    }

    /// Generates and links an object file of `self.cx` using LLVM's machine code generator
    /// Object file intermediaries are written to /tmp, linked using clang, and removed
    pub fn generate(&self, filename: &str) {
        let mut error = ptr::null_mut();
        let cfilename = CString::new(format!("/tmp/{}.o", filename)).unwrap();

        unsafe {
            let ret = LLVMTargetMachineEmitToFile(self.target_machine, self.cx.llmod, cfilename.as_ptr() as *mut i8, LLVMCodeGenFileType::LLVMObjectFile, &mut error);

            if ret == 1 {
                let x = CStr::from_ptr(error);
                LLVMDisposeMessage(error);
                panic!("Unable to generate file: {}", x.to_string_lossy());
            }
        }

        let ld_output = Command::new("/usr/bin/clang").arg(format!("/tmp/{}.o", filename))
            .arg("-o")
            .arg(filename)
            .output()
            .unwrap();

        if !ld_output.status.success() {
            eprintln!("Compilation failed at linking");
            eprintln!("{}", String::from_utf8_lossy(&ld_output.stderr[..]));
        }

        fs::remove_file(format!("/tmp/{}.o", filename)).unwrap();
    }
}
